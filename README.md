About: The scripts in this repository corrects the frequent grammatical errors using classification. The feature vectors for the classsification include previous 2 words, next 2 words, previous 2 POS tags and next 2 POS tags along with the word and its confused pair.

Applied Natural Language Processing (CSCI 544) Home work - 3.

Author: Dharmik

Date: March 15th, 2015

Contents:

src/generate_train_file.py : This scripts takes the TRAINFILE as argument and generates the training file containing feature vectors. The generated file name would be fmtd_TRAINFILE.
USAGE: python generate_train_file.py TRAINFILE

src/generate_test_file.py : This scripts takes the TESTFILE as argument and generates the test file containing feature vectors, order file which contains the positions of the words to be corrected which is later used to merge the predicted words with the original test file. The generated file names would be test_fmtd_TESTFILE and test_order_TESTFILE.
USAGE: python generate_test_file.py TESTFILE

src/generate_op_file.py : This script takes the TESTFILE, ORDERFILE and PREDICTEDFILE as arguments and generates the op_TESTFILE as the final output which contains the sentences with possible corrected words.
USAGE: python3 generate_op_file.py TESTFILE ORDERFILE PREDICTEDFILE

hw3.output.txt : This is the output file with possible corrected words after running the scripts on the test file provided.


How it works?

1) "generate_train_file.py" takes the TRAINFILE which contains the sentences, tags them using NLTK POS Tagger and generates a feature vector for the classes ("it's", "its", "you're", "your", "they're", "their", "loose", "lose", "to", "too"). The feature vector includes previous 2 words, next 2 words, previous 2 POS tags and next 2 POS tags of the word (mentioned above) to be predicted.

2) The generated file with feature vectors are trained against *megam* and a model file is generated.
USAGE: megam -nc multiclass fmtd_TRAINFILE > test.megam.model

3) We classify the test file containing feature vectors against the above generated model file with megam and generate a PREDICTEDFILE containing the possible correct words for the list of above wrongly used words.
USAGE: megam -predict test.megam.model multiclass test_fmtd_TESTFILE > PREDICTEDFILE

4) The PREDICTEDFILE along with the test_order_TESTFILE and the TESTFILE is used to generate the op_TESTFILE using the generate_op_file.py which would replace the wrongly used words in TESTFILE with predicted correct words from the PREDICTEDFILE.
