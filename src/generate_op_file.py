import re
import sys

word_pair = {
    "it's" : "its",
    "its" : "it's",
    "you're" : "your",
    "your" : "you're",
    "they're" : "their",
    "their" : "they're",
    "loose" : "lose",
    "lose" : "loose",
    "to" : "too",
    "too" : "to"
}

def get_predicted_word(pred_content, w, class_idx):
    # If the predicted class is a pair, return the class.
    if pred_content[0] == w or pred_content[0] == word_pair[w]:
        return pred_content[0]
    print (pred_content)
    print (w)
    # Else return that word from its pair that has maximum predicted probability.
    if float(pred_content[class_idx[w]]) >= float(pred_content[class_idx[word_pair[w]]]):
        return w
    else:
        return word_pair[w]

def read_order_file(order_file, tag_file, class_idx):
    # Format of the order file is as follows.
    # Line# word word_index#
    file_order = open(order_file, "r", errors="ignore")
    file_tag = open(tag_file, "r", errors="ignore")
    word_map = {}
    count = 0
    for f1, f2 in zip(file_order.readlines(), file_tag.readlines()):
        l, w, idx = f1.strip().split()
        new_w = get_predicted_word(f2.strip().split(), w, class_idx)
        #print (l,w,idx)
        #print (new_w)
        l = int(l)
        idx = int(idx)
        # Check for the tag and w, such that they belong to the same pair
        # We just skip it if it doesn't match with its pair.
        if (w == new_w or word_pair[w] != new_w):
            continue
        count += 1
        if l in word_map:
            word_map[l].append((w, idx, new_w))
        else:
            word_map[l] = [(w,idx, new_w)]
    file_order.close()
    print ("Total changes in the test file:" + str(count))
    return word_map
    

def get_edited_line(line, word_list):
    prev = 0
    new_line = []
    line_words = line.split(" ")
    for w,idx,new_w in word_list:
        new_line += line_words[prev:idx] + [new_w]
        prev = idx + 1
    new_line += line_words[prev:]
    return " ".join(new_line)

def get_class_idx(model_file):
    classes = open(model_file, "r", errors ="ignore").readline().split()
    class_idx = {}
    for k in classes[1:]:
        class_idx[k] = classes.index(k)
    return class_idx
    
def main(test_file, order_file, tag_file, model_file):
    class_idx = get_class_idx(model_file)
    word_map = read_order_file(order_file, tag_file, class_idx)
    f_ip = open(test_file, "r", errors="ignore")
    f_op = open("op_" + test_file, "w", errors="ignore")
    f_uc = open("unchanged_" + test_file, "w", errors="ignore")
    f_c = open("changed_" + test_file, "w", errors="ignore")
    f_w = open("wrong_" + test_file, "w", errors="ignore")
    line_count = 0
    for l in f_ip.readlines():
        line_count += 1
        l = l.strip()
        # If no change exists for a given line,
        # print and continue to the next line.
        if line_count not in word_map:
            f_op.write (l + "\n")
            f_uc.write (l + "\n")
            continue
        changed_line = get_edited_line(l, word_map[line_count])
        f_op.write(changed_line + "\n")
        f_c.write(changed_line + "\n")
        f_w.write(l + "\n")
    f_ip.close()
    f_op.close()
    print (class_idx)
        

if __name__ == "__main__":
    if len(sys.argv) < 5:
        print ("This script takes 4 args.")
        print ("USAGE: python3 generate_op_file.py TESTFILE test_order_TESTFILE PREDICTFILE MODELFILE")
        exit()
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
