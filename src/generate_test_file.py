import codecs
import nltk
import sys

word_class = ["it's", "its", "you're", "your", "they're", "their",
              "loose", "lose", "to", "too"]

word_pair = {
    "it's" : "its",
    "its" : "it's",
    "you're" : "your",
    "your" : "you're",
    "they're" : "their",
    "their" : "they're",
    "loose" : "lose",
    "lose" : "loose",
    "to" : "too",
    "too" : "to"
}

def get_word_indices(word_list, word_ids, tagged):
    indices = []
    idx = 0
    # The elements of tagged word_list are tuple
    # while in non tagged word_list, they are just words.
    if tagged == 1:
        # If the words are tagged, take the 1st element of tuple.
        for w in word_list:
            if w[0] in word_ids:
                indices.append(idx)
            idx += 1
    else:
        for w in word_list:
            if w in word_ids:
                indices.append(idx)
            idx += 1
    return indices


def main(file_ip):
    f_ip = codecs.open(file_ip, "r", errors="ignore")
    f_op = codecs.open("test_fmtd_" + file_ip, "w", errors="ignore")
    f_order = codecs.open("test_order_"+ file_ip, "w", errors="ignore")
    count = 0
    for l in f_ip.readlines():
        if (count % 10000 == 0):
            print ("Done with line " + str(count))
        count += 1
        line = l.strip()        
        line_words = line.split(" ")
        # Get the list of words to be checked in the current line.
        word_ids = list(set(line_words) & set(word_class))
        if len(word_ids) <= 0:
            # There are no words which should be checked for.
            # Skip this line.
            continue
        # Save the line, word and its index in the order file for merging
        # the output with the input file.
        untagged_indices = get_word_indices(line_words, word_ids, 0)
        for idx in untagged_indices:
            f_order.write(str(count) + " " + line_words[idx] + " " + str(idx) + "\n")
        
        # Tag the words.
        tagged_words = nltk.pos_tag(line_words)
        # Insert the BOS and EOS tags.
        tagged_words.insert(0, ("BOS1", "BOS1"))
        tagged_words.insert(0, ("BOS0", "BOS0"))
        tagged_words.append(("EOS1", "EOS1"))
        tagged_words.append(("EOS0", "EOS0"))
        
        # Get the index of the word which should be checked after tagging.
        # We do it again because the tagger splits the words with "n't"
        word_indices = get_word_indices(tagged_words, word_ids, 1)
        for idx in word_indices:
            # Create a feature for the previous 2 words and the next 2 words
            # along with its POS tags.
            prev_word2, prev_tag2 = tagged_words[idx-2]
            prev_word1, prev_tag1 = tagged_words[idx-1]
            cur_word, cur_tag = tagged_words[idx]
            next_word1, next_tag1 = tagged_words[idx+1]
            next_word2, next_tag2 = tagged_words[idx+2]
            tag_seq = " tseq_" + "_".join([prev_tag2, prev_tag1, next_tag1, next_tag2])
            word_seq = " wseq_" + "_".join([prev_word2, prev_word1, next_word1, next_word2])
            # Get word classes.
            word_features = ""
            if cur_word > word_pair[cur_word]:
                word_features = "c1_" + cur_word + " c2_" + word_pair[cur_word] 
            else:
            	word_features = "c1_" + word_pair[cur_word] + " c2_" + cur_word
            f_op.write(word_features + " pw2_" + prev_word2 +
                       " pw1_" + prev_word1 + " nw1_" + next_word1 + 
                       " nw2_" + next_word2 + " pt2_" + prev_tag2 +
                       " pt1_" + prev_tag1 + " nt1_" + next_tag1 +
                       " nt2_" + next_tag2 + word_seq + tag_seq + "\n")
    f_ip.close()
    f_op.close()
    f_order.close()

if __name__== "__main__":
    main(sys.argv[1])
